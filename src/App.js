import React,{useState} from 'react';
import './App.css';
import Main from './components/Main';
import Home from './components/Home';
import Book from './components/Book';
import Account from './components/Account';
import Auth from './components/Auth';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router,Switch,Route, Redirect} from 'react-router-dom';
import Logo from './components/Logo';
import SeeMore from './components/SeeMore';
import NotFound from './components/NotFound';
import Welcome from './components/Welcome';

function App() {
  const [auth] = useState(true);
  return (
    <div className="App">
      <Router>
        <Main />
        <Switch>
          <Route path="/" exact component={Logo} />
          <Route path="/home" component={Home} />
          <Route path="/book" component={Book} />
          <Route path="/account" component={Account} />
          <Route path="/auth" component={Auth} />
          <Route path="/seemore/:id" component={SeeMore} />
          <PrivateRoute path="/welcome" component={Welcome} auth={auth} />
          <Route path="*" component={NotFound} />
        </Switch>
      </Router>
    </div>
  );
}

const PrivateRoute = ({auth,component: Component, ...rest}) => {
  return(
    <Route {...rest} 
      render={
        props => auth? (<Component {...props}/>)
        :(<Redirect to={{pathname: "/auth"}} />)
      }
    />
  )
}

export default App;
