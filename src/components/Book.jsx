import React, { Component } from "react";
import { Link, Route, Switch, BrowserRouter as Router } from "react-router-dom";
import Ebook from "./Ebook";
import CopiedBook from "./CopiedBook";

export default class Book extends Component {
  render() {
    return (
      <div className="container">
        <h1>BOOK</h1>
        <div className="row">
          <div
            className="col-md-5"
            style={{ textAlign: "left", margin: "0 auto" }}
          >
            <Router>
            <h2>Type of Books</h2>
              <ul>
                <Link to="/book/ebook">
                  <li>E-Book</li>
                </Link>
                <Link to="/book/copiedbook">
                  <li>Copied Book</li>
                </Link>
              </ul>
              <Switch>
                <Route path="/book/ebook" component={Ebook} />
                <Route path="/book/copiedbook" component={CopiedBook} />
                <Route path="/book" exact render={()=> <h3 style={{color: "maroon"}}>Choose Something!</h3>} />
              </Switch>
            </Router>
          </div>
        </div>
      </div>
    );
  }
}
