import React, { Component } from "react";
import { Card, Button } from "react-bootstrap";
import {Link} from 'react-router-dom';

export default class Home extends Component {
  constructor() {
    super();
    this.state = {
      book: [
        {
          id: 1,
          image: "https://www.askewsandholts.com/AskHolts/images/bookph180.jpg",
          title: "Khmer",
          description:
            "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.",
        },
        {
          id: 2,
          image:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRIZd1plxAEIm6p5gwf2uyk-RJlAqLNSKf2SkKW1oxsreWne9cB&usqp=CAU",
          title: "History",
          description:
            "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.",
        },
        {
          id: 3,
          image:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS6pBe_UWOdRKT9VuEk9-QKWL7ULv0MXLvKIl-bdKQ7hUVU7mr8&usqp=CAU",
          title: "Physics",
          description:
            "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.",
        },
        {
          id: 4,
          image:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTbN6ktKWPwC6Q7MeCs_-gjBaMgSQMIJ15Tfq-61d4JK_Xj2ERZ&usqp=CAU",
          title: "Chemistry",
          description:
            "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.",
        },
        {
          id: 5,
          image:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQcr5-ej_9VNRE8k7XPFhMKiC893kZi_XmVOEEthTRQKhm8wWUf&usqp=CAU",
          title: "Science",
          description:
            "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.",
        },
        {
          id: 6,
          image:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcQA6aYBKp64fzyuPRwr8xlGmHyD7oek6jRuPawOvUtC7qWjj0n3&usqp=CAU",
          title: "Geography",
          description:
            "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.",
        },
        {
          id: 7,
          image:
            "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcTtVwtM4PWei-jP2w6zTq78E5ulKuMxYVMk6-hbRBtQ_xkiKhXO&usqp=CAU",
          title: "Technology",
          description:
            "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.",
        },
        {
          id: 8,
          image:
            "https://c0.wallpaperflare.com/preview/410/669/124/black-android-smartphone-with-plugged-in-earphones-on-book.jpg",title: "Psychology",
          description:
            "Lorem ipsum, or lipsum as it is sometimes known, is dummy text used in laying out print, graphic or web designs.",
        },
      ],
    };
  }

  render() {
    let mybook = this.state.book.map((b) => (
      <div className="col-md-3" key={b.id}>
        <Card style={{ width: "16rem", margin: '0 auto 25px'}}>
          <Card.Img variant="top" src={b.image} height="175px" />
          <Card.Body>
            <Card.Title>{b.title}</Card.Title>
            <Card.Text>{b.description}</Card.Text>
            <Link style={{color: "white"}} to={{pathname: `/seemore/${b.id}`, book: {myimage: `${b.image}`,mytitle: `${b.title}`, mydesc: `${b.description}`},}}>
                <Button variant="primary">See More</Button></Link>
          </Card.Body>
        </Card>
      </div>
    ));
    return (
      <div className="container">
        <h1>HOME</h1>
        <div className="row">{mybook}</div>
      </div>
    );
  }
}
