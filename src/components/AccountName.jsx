import React, { Component } from 'react'
import queryString from 'query-string';

export default class AccountName extends Component {
    render() {
        let query = queryString.parse(this.props.location.search);
        let getQuery = `${query.name}`.toUpperCase();
        if(getQuery === 'UNDEFINED'){
            getQuery = "NOTHING";
        }
        return (
            <div>
                <h3>Account <span style={{color: 'maroon'}}>name</span> is "{getQuery}".</h3>
            </div>
        )
    }
}
