import React, { Component } from "react";
import {Card} from 'react-bootstrap';

export default class SeeMore extends Component {
  render() {
    let image = "";
    let title = "";
    let desc = "";
      try {
         image = this.props.location.book.myimage;
         title = this.props.location.book.mytitle;
         desc = this.props.location.book.mydesc;
      } catch (error) {
          image = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS5YznahbCcaCBgKJ4j6nYvPHTYEt5MX9tAaQlrXnMjPNKtQVS6&usqp=CAU";
          title = "Try again!";
      }
   
    return (
      <div className="container">
        <h1>SEE MORE {this.props.match.params.id}</h1>
        <div className="row">
          <div className="col-md-4" style={{margin: '0 auto'}}>
            <Card style={{ width: "18rem", margin: '0 auto' }}>
              <Card.Img variant="top" src={image} height="175px" />
              <Card.Body>
                <Card.Title>{title}</Card.Title>
                <Card.Text>{desc}</Card.Text>
              </Card.Body>
            </Card>
          </div>
        </div>
      </div>
    );
  }
}
