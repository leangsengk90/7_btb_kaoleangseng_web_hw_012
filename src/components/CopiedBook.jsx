import React, { Component } from "react";
import { Link, Route, Switch, BrowserRouter as Router } from "react-router-dom";
import Science from "./category/Science";
import Geography from "./category/Geography";
import Technology from "./category/Technology";
import Psychology from "./category/Psychology";
import Home from "./Home";

export default class CopiedBook extends Component {
  render() {
    const home = new Home();
    return (
      <div>
        <Router>
          <h3>Copied Book Category</h3>
          <ul>
            <Link
              to={{
                pathname: "/book/copiedbook/science",
                book: {
                  image: home.state.book[4].image,
                  desc: home.state.book[4].description,
                },
              }}
            >
              <li>Science</li>
            </Link>
            <Link
              to={{
                pathname: "/book/copiedbook/geography",
                book: {
                  image: home.state.book[5].image,
                  desc: home.state.book[5].description,
                },
              }}
            >
              <li>Geography</li>
            </Link>
            <Link
              to={{
                pathname: "/book/copiedbook/technology",
                book: {
                  image: home.state.book[6].image,
                  desc: home.state.book[6].description,
                },
              }}
            >
              <li>Technology</li>
            </Link>
            <Link
              to={{
                pathname: "/book/copiedbook/psychology",
                book: {
                  image: home.state.book[7].image,
                  desc: home.state.book[7].description,
                },
              }}
            >
              <li>Psychology</li>
            </Link>
          </ul>
          <Switch>
            <Route
              path="/book/copiedbook/"
              exact
              render={() => (
                <h3 style={{ color: "darkgreen" }}>Choose Something!</h3>
              )}
            />
            <Route path="/book/copiedbook/:name" component={Science} />
            <Route path="/book/copiedbook/:name" component={Geography} />
            <Route path="/book/copiedbook/:name" component={Technology} />
            <Route path="/book/copiedbook/:name" component={Psychology} />
          </Switch>
        </Router>
      </div>
    );
  }
}
