import React, { Component } from "react";
import { Link, Route, Switch, BrowserRouter as Router } from "react-router-dom";
import Khmer from "./category/Khmer";
import Physics from "./category/Physics";
import Chemistry from "./category/Chemistry";
import History from "./category/History";
import Home from "./Home";

export default class Ebook extends Component {
  render() {
    const home = new Home();
    return (
      <div>
        <Router>
          <h3>E-Book Category</h3>
          <ul>
            <Link
              to={{
                pathname: "/book/ebook/khmer",
                book: { image: home.state.book[0].image,
                  desc: home.state.book[0].description
                 },
              }}
            >
              <li>Khmer</li>
            </Link>
            <Link to={{
                pathname: "/book/ebook/history",
                book: { image: home.state.book[1].image,
                  desc: home.state.book[1].description
                 },
              }}
            >
              <li>History</li>
            </Link>
            <Link to={{
                pathname: "/book/ebook/physics",
                book: { image: home.state.book[2].image,
                  desc: home.state.book[2].description
                 },
              }}
            >
              <li>Physics</li>
            </Link>
            <Link to={{
                pathname: "/book/ebook/chemistry",
                book: { image: home.state.book[3].image,
                  desc: home.state.book[3].description
                 },
              }}
            >
              <li>Chemistry</li>
            </Link>
          </ul>
          <Switch>
            <Route
              path="/book/ebook/"
              exact
              render={() => (
                <h3 style={{ color: "orange" }}>Choose Something!</h3>
              )}
            />
            <Route path="/book/ebook/:name" component={Khmer} />
            <Route path="/book/ebook/:name" component={History} />
            <Route path="/book/ebook/:name" component={Physics} />
            <Route path="/book/ebook/:name" component={Chemistry} />
          </Switch>
        </Router>
      </div>
    );
  }
}
