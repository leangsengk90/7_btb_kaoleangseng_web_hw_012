import React, { Component } from "react";
import AccountName from "./AccountName";
import { Link, Route, Switch, BrowserRouter as Router } from "react-router-dom";

export default class Account extends Component {
  render() {
    return (
      <div className="container">
        <h1>ACCOUNT</h1>
        <div className="row">
          <div
            className="col-md-5"
            style={{ textAlign: "left", margin: "0 auto" }}
          >
            <Router>
              <ul>
                <Link to="/account?name=facebook">
                  <li>Facebook</li>
                </Link>
                <Link to="/account?name=twitter">
                  <li>Twitter</li>
                </Link>
                <Link to="/account?name=telegram">
                  <li>Telegram</li>
                </Link>
                <Link to="/account?name=instagram">
                  <li>Instagram</li>
                </Link>
              </ul>
              <Switch>
                <Route path="/account" component={AccountName} />
              </Switch>
            </Router>
          </div>
        </div>
      </div>
    );
  }
}
