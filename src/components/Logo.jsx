import React, { Component } from "react";

export default class Logo extends Component {
  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-md-12">
            <h1>LIBRARY</h1>
            <img
              src="https://www.askewsandholts.com/AskHolts/images/bookph3.jpg"
              height="500px"
            />
          </div>
        </div>
      </div>
    );
  }
}
