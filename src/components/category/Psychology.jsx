import React, { Component } from 'react';
import {Card} from 'react-bootstrap';

export default class Psychology extends Component {
    render() {
        let image = "";
        let desc = "";
        try {
          image = this.props.location.book.image;
          desc = this.props.location.book.desc;
       } catch (error) {
           image = "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcS5YznahbCcaCBgKJ4j6nYvPHTYEt5MX9tAaQlrXnMjPNKtQVS6&usqp=CAU";
           desc = "Try again!";
       }
      return (
          <div className="row">
            <div className="col-md-12" style={{ margin: "0 auto", textAlign: 'center' }}>
            <h3>{this.props.match.params.name}</h3>
              <Card style={{ width: "18rem", margin: "0 auto" }}>
                <Card.Img variant="top" src={image} height="175px" />
                <Card.Body>
                  <Card.Text>{desc}</Card.Text>
                </Card.Body>
              </Card>
            </div>
          </div>
      );
    }
}
