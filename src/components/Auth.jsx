import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import { Redirect, Route, BrowserRouter as Router } from "react-router-dom";
import '../App.css';

export default class Auth extends Component {
  constructor() {
    super();
    this.state = {
      loggedIn: false,
      email: "",
      pass: "",
      sms: "",
    };
  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  loggedInHandle = () => {
    this.setState(() => {
      if (this.state.email !== "" && this.state.pass !== "") {
        return this.state.loggedIn = true;
      }else{
        return this.state.sms = "Please input something!";
      }
    });
  };

  render() {
    if (this.state.loggedIn) {
      return <Redirect to={{ pathname: "/welcome", state: { term: true, email: this.state.email } }} />;
    }
    return (
      <div className="container">
        <h1>AUTHENTICATION</h1>
        <div className="row">
          <div
            className="col-md-6"
            style={{ textAlign: "left", margin: "0 auto" }}
          >
            <Form>
              <Form.Group controlId="formBasicEmail">
                <Form.Label>Email</Form.Label>
                <Form.Control
                  type="email"
                  placeholder="Enter email"
                  name="email"
                  onChange={this.handleChange.bind(this)}
                />
                <Form.Text className="sms-text" >{this.state.email===""?this.state.sms:""}</Form.Text>
              </Form.Group>

              <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type="password"
                  placeholder="Enter password"
                  name="pass"
                  onChange={this.handleChange.bind(this)}
                />
                <Form.Text className="sms-text" >{this.state.pass===""?this.state.sms:""}</Form.Text>
              </Form.Group>
              <Button
                variant="primary"
                type="button"
                style={{ width: "200px" }}
                onClick={this.loggedInHandle.bind(this)}
              >
                Submit
              </Button>
            </Form>
          </div>
        </div>
      </div>
    );
  }
}
