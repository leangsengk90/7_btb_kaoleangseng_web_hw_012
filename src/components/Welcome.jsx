import React, { Component } from 'react'
import {Redirect} from 'react-router-dom';

export default class Welcome extends Component {
    render() {
        let term = false;
        let email = "";
        try {
          term = this.props.location.state.term;
          email = this.props.location.state.email;
        } catch (error) {}
        console.log("WELCOME: "+term);
        if(term === false){
            return (<Redirect to="/auth"/>)
        }
        return (
            <div>
                <h1>Welcome</h1>
                <h2 style={{color: 'rgb(0,162,181)'}}>{email}</h2>
            </div>
        )
    }
}
